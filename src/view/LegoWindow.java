package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Observer;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
import model.DirectionHistory;

public class LegoWindow extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Dimension screenSize;
	MQTTPanel mQTTPanel;
	HistoryPanel historyPanel;
	JPanel browserPanel;
	JWebBrowser webBrowser;
	
	public LegoWindow(ArrayList<DirectionHistory> directionHistoryLog) {
		screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize((int)(screenSize.getWidth()), (int)screenSize.getHeight());

		this.mQTTPanel = new MQTTPanel((int)(this.getWidth()*0.8), (int)(this.getHeight()));
		this.historyPanel = new HistoryPanel((int)(this.getWidth()*0.2), (int)(this.getHeight()), directionHistoryLog);
		this.setLayout(new BorderLayout());
		this.setExtendedState(JFrame.MAXIMIZED_VERT); 
		this.add(mQTTPanel, BorderLayout.WEST);
		this.add(historyPanel, BorderLayout.EAST);
		this.setUndecorated(true);
		this.setVisible(true);
	}
	
	public void addMQTTPanel(ArrayList<NodeView> mQTTNodeViewList, CopyOnWriteArrayList<PacketView> mQTTPacketViewList) {
		mQTTPanel.setNodeViewList(mQTTNodeViewList);
		mQTTPanel.setPacketViewList(mQTTPacketViewList);
	}
	
	public void addBrowserPanel(String address) {
	    webBrowser = new JWebBrowser();
	    webBrowser.setBarsVisible(false);
	    webBrowser.navigate(address);
	    
	    webBrowser.setPreferredSize(new Dimension(browserPanel.getWidth(), browserPanel.getHeight()));
	    browserPanel.add(webBrowser);
	    this.revalidate();
		System.out.println("browser created");
	}

	public void repaintMap() {
		if(mQTTPanel != null) {
			mQTTPanel.repaint();
		}
	}

	public void addObserver(Observer controller) {
		historyPanel.setObserver(controller);
	}

}
