package view;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;

import controller.Controller;

public class MQTTPanel extends MapPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Image cloud;
	private Image stack;
	private Image truckImage;

	public MQTTPanel(int width, int height) 
	{
		super(width, height);
		this.cloud = new ImageIcon("textures/" + "Cloud" + ".png").getImage();
		cloud = cloud.getScaledInstance((int) (width*0.5), -1, Image.SCALE_SMOOTH);

		this.stack = new ImageIcon("textures/" + "mqttStack" + ".png").getImage();
		stack = stack.getScaledInstance((int) (stack.getWidth(null)*0.5), -1, Image.SCALE_SMOOTH);
		
		this.truckImage = new ImageIcon("textures/" + "legoTruck2" + ".png").getImage();
//		truckImage = truckImage.getScaledInstance((int) (truckImage.getWidth(null)), -1, Image.SCALE_SMOOTH);
	}
	
	@Override
	public void drawExtra(Graphics g) {
		int cloudX = (int)( (this.getWidth()*0.5) - (cloud.getWidth(null)/2.0));
		int cloudY = (int)(this.getHeight()*0.02);
		g.drawImage(cloud, cloudX, cloudY, null);
		g.drawImage(stack, (int) (cloudX + (cloud.getWidth(null)*0.7)), (int)(cloudY + (cloud.getHeight(null)*0.3)), null);
		double leftOfCarValue = (this.getWidth()*(Controller.CARX + 0.12));
		double truckImageX = (truckImage.getWidth(null)/2.0);
		double carY = (this.getHeight()*Controller.CARY);
		double truckImageY = (truckImage.getHeight(null)/2.0);
		g.drawImage(truckImage, 
					(int)(leftOfCarValue - truckImageX),
					(int)(carY - truckImageY),
					null);
	}

}
