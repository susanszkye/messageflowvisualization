package view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class MapPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	protected ArrayList<NodeView> nodeViewList;
	protected CopyOnWriteArrayList<PacketView> packetViewList;

	public MapPanel(int width, int height) {
		this.nodeViewList = null;
		this.packetViewList = null;
		this.setPreferredSize(new Dimension(width, height));
	}
	
	public void drawExtra(Graphics g){
		
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.drawExtra(g);
		
		if(nodeViewList != null) {
			for(NodeView nodeView: nodeViewList) {
				nodeView.draw(g, this.getSize());
				//System.out.println("draw node");
			}
		}
		
		if(packetViewList != null) {
			for(PacketView packetView: packetViewList) {
				packetView.draw(g, this.getSize());
			}
		}
	}
	
	public void setNodeViewList(ArrayList<NodeView> nodeViewList) {
		this.nodeViewList = nodeViewList;
	}
	
	public void setPacketViewList(CopyOnWriteArrayList<PacketView> packetViewList) {
		this.packetViewList = packetViewList;
	}

}
