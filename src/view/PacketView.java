/**
 * 
 */
package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;

import controller.Controller;
import model.Packet;
import model.Packet.BodyValue;
import model.Packet.Type;

/**
 * @author eeszsus
 *
 */
public class PacketView implements Observer {
	
	Packet packet;
	ArrayList<NodeView> nodeViewList;
//	Integer currentNode;
	private NodeView currentNodeView;
	private Image currentImage;
	private HashMap<Type, Image> protocolImageMap;
	Image httpNoHeader;
	private Image directionImage;
	private Coordinate position;
	private Controller controller;
	double distanceDelta;
	double xDelta;
	double yDelta;
	double moveDistance;
	
	public PacketView(Packet packet, ArrayList<NodeView> nodeViewList, Controller controller, double moveDistance, int currentNode) {
		this.packet = packet;
		this.nodeViewList = nodeViewList;
		this.currentNodeView = this.nodeViewList.get(currentNode);
//		position = new Coordinate(nodeViewList.get(currentNode).getPositionX(), nodeViewList.get(currentNode).getPositionY());
		position = new Coordinate(currentNodeView.getPositionX(), currentNodeView.getPositionY());
		this.controller = controller;
		
		this.moveDistance = moveDistance;
		this.setDelta();
		this.setDistanceDelta();
		
		this.packet.addObserver(this);

		protocolImageMap = new HashMap<Type, Image>();
		protocolImageMap.put(Type.HTTP, new ImageIcon("textures/" + "HTTP" + "_small.png").getImage());
		protocolImageMap.put(Type.MQTT, new ImageIcon("textures/" + "MQTT" + "_small.png").getImage());
		httpNoHeader = new ImageIcon("textures/" + "HTTP" + "_resp.png").getImage();
	    
		currentImage = protocolImageMap.get(packet.getType());
		
		directionImage = new ImageIcon("textures/" + packet.getDirection() + "_small.png").getImage();
	}
	
	private void setDistanceDelta() {
		this.distanceDelta = currentNodeView.getDistance() / moveDistance; //nodeViewList.get(currentNode).getDistance() / moveDistance;
	}

	private void setDelta() {
		this.setDistanceDelta();
		NodeView nextNodeView = currentNodeView.getNextNodeView();//nodeViewList.get(currentNode + 1);
//		NodeView currentNodeView = nodeViewList.get(currentNode);
		if(nextNodeView != null) {
			this.xDelta = (currentNodeView.getPositionX() - nextNodeView.getPositionX()) / distanceDelta;
			this.yDelta = (currentNodeView.getPositionY() - nextNodeView.getPositionY()) / distanceDelta;
		}
	}

	public void draw(Graphics g, Dimension panelSize) {
		
		int x = (int) Math.floor(position.getX() * panelSize.getWidth());
		int y = (int) Math.floor(position.getY() * panelSize.getHeight());

		int imageHeight = 0;
		int imageWidth = 0;
		if(currentImage != null) {
			imageHeight = currentImage.getHeight(null);
			imageWidth = currentImage.getWidth(null);
			
			//String value = packet.getSensorValue();
		    g.drawImage(directionImage, x - imageWidth/2, y+2, null);
			g.drawImage(currentImage, x - imageWidth/2, y - imageHeight/2, null);
//			if( value != null) {
//				g.setColor(Color.WHITE);
//				g.setFont(new Font("Arial Black", Font.PLAIN, 12));
//				String sensorValue = packet.getSensorValue();
//				switch (packet.getBodyValue()) {
//				case TEMP:
//					int dotAt = sensorValue.indexOf(".");
//					sensorValue = "temp: " + sensorValue.substring(0, dotAt + 2);
//					break;
//				case DROP:
//					if(sensorValue == "0.0") {
//						sensorValue = "False";
//					} else {
//						sensorValue = "True";
//					}
//					sensorValue = "dropped: " + sensorValue;
//					break;
//				default:
//					break;
//				}
//
//				g.drawString(sensorValue, x - imageWidth/2 + 10, y + imageHeight/4 + 5);
//			}
//			g.drawOval(x, y, 5, 5);
		}
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		NodeView nextNodeView = currentNodeView.getNextNodeView();
		if(nextNodeView.getNextNodeView() == null) {
			controller.deletePacketView(this);
		} else {
			currentNodeView = nextNodeView;
			double newDistance = currentNodeView.getDistance();	//nodeViewList.get(currentNode).getDistance();
			packet.setDistance(newDistance);
			setDelta();
			currentImage = protocolImageMap.get(packet.getType());
		}
	}
	
	public void newPosition() {
		if(currentNodeView.getNextNodeView() != null) {
			double percent = 1-(packet.getDistance() / currentNodeView.getDistance());	//nodeViewList.get(currentNode).getDistance());
			double acceleration = (2 - Math.abs(4 * (percent - 0.5)));
			double x = xDelta * acceleration;
			double y = yDelta * acceleration;
			position.setDelta(x, y);
		}
	}
}
