/**
 * 
 */
package view;

/**
 * @author eeszsus
 *
 */
public class Coordinate {
	private double x;
	private double y;
	
	public Coordinate(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		// TODO Auto-generated method stub
		return x;
	}
	
	public double getY() {
		// TODO Auto-generated method stub
		return y;
	}
	
	public double distance(Coordinate c) {
		return Math.sqrt(Math.pow(x-c.getX(), 2) + Math.pow(y-c.getY(), 2));
	}
	
	public double angle(Coordinate c) {
		final double deltaY = (this.getY() - c.getY());
	    final double deltaX = (this.getX() - c.getX());
	    final double result = Math.toDegrees(Math.atan2(deltaY, deltaX));
	    return (result < 0) ? (360d + result) : result;
	}

	public void setDelta(double xDelta, double yDelta) {
		this.x -= xDelta;
		this.y -= yDelta;
	}
}
