package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JPanel;

import model.DirectionHistory;

public class HistoryPanel extends JPanel {

	JButton sendHistoryButton;
	final MessageObservable observable = new MessageObservable();
	private ArrayList<DirectionHistory> directionHistoryLog;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HistoryPanel(int width, int height, ArrayList<DirectionHistory> directionHistoryLog) {
		this.setPreferredSize(new Dimension(width, height));
		this.sendHistoryButton = new JButton("History");
		
		sendHistoryButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				observable.changeData();
			}
		});
		
		this.sendHistoryButton.setPreferredSize(new Dimension(300, 100));
		this.setLayout(new BorderLayout());
		JPanel flowPanel = new JPanel(new FlowLayout());
		flowPanel.add(sendHistoryButton);
		this.add(flowPanel, BorderLayout.CENTER);
//		this.revalidate();
	}
	
	class MessageObservable extends Observable {
	    MessageObservable() {	
	        super();
	    }
	    void changeData() {
	        setChanged(); // the two methods of Observable class
	        notifyObservers();
	    }
	}

	public void setObserver(Observer controller) {
		observable.addObserver(controller);
	}
	
//	public void draw(Graphics g, Dimension panelSize) {
//		if(directionHistoryLog != null) {
//		g.setColor(Color.WHITE);
//		g.setFont(new Font("Arial Black", Font.PLAIN, 12));
//		String sensorValue = packet.getSensorValue();
//		switch (packet.getBodyValue()) {
//		case TEMP:
//			int dotAt = sensorValue.indexOf(".");
//			sensorValue = "temp: " + sensorValue.substring(0, dotAt + 2);
//			break;
//		case DROP:
//			if(sensorValue == "0.0") {
//				sensorValue = "False";
//			} else {
//				sensorValue = "True";
//			}
//			sensorValue = "dropped: " + sensorValue;
//			break;
//		default:
//			break;
//		}
//
//		g.drawString(sensorValue, x - imageWidth/2 + 10, y + imageHeight/4 + 5);
//		}
//	}
}
