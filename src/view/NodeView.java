/**
 * 
 */
package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;

import model.Node;

/**
 * @author eeszsus
 *
 */
public class NodeView implements Observer {
	private NodeView nextNodeView;
	private String name;
	private Coordinate position;
	private Coordinate nextPosition;
	private Image image;
	private double distance;

	public NodeView(Node node, Coordinate position) {
		this.distance = 0;
		this.name = node.getName();
		this.position = position;
		this.nextNodeView = null;
		image = null;
		image = new ImageIcon("textures/" + name + ".png").getImage();
		int imageH = image.getHeight(null);
		int imageW = image.getWidth(null);
		if(imageH > 0 && imageW > 0) {
			image = image.getScaledInstance((int)(imageW*0.8), (int)(imageH*0.8), Image.SCALE_SMOOTH);
		}
	}

	public double getDistance() {
		if(nextNodeView != null) {
			return position.distance(nextPosition);
		} else {
			return 0;
		}
	}
	
	public Coordinate getPosition() {
		return position;
	}

	public void setNextNodeView(NodeView nextNodeView) {
		this.nextNodeView = nextNodeView;
		this.nextPosition = nextNodeView.getPosition();
		this.distance = getDistance();
	}
	
	public void draw(Graphics g, Dimension panelSize) {
		int x = (int) (position.getX() * panelSize.getWidth());
		int y = (int) (position.getY() * panelSize.getHeight());

		int imageHeight = 0;
		int imageWidth = 0;
		if(image != null) {
			imageHeight = image.getHeight(null);
			imageWidth = image.getWidth(null);
		}
		
		if(nextPosition != null) {
			int lineX = (int) (nextPosition.getX() * panelSize.getWidth());
			int lineY = (int) (nextPosition.getY() * panelSize.getHeight());
			g.setColor(new Color(95, 186, 224));
			g.drawLine(x, y, lineX, lineY);
		}
		
		g.drawImage(image, x - imageWidth/2, y - imageHeight/2, null);
	}

	public String getName() {
		return name;
	}

	public double getPositionX() {
		return position.getX();
	}

	public double getPositionY() {
		return position.getY();
	}

	public NodeView getNextNodeView() {
		return nextNodeView;
	}

	@Override
	public void update(Observable o, Object arg) {
		System.out.println("arg: " + arg.toString());
		if(arg.toString() == "white") {
			image = new ImageIcon("textures/" + name + ".png").getImage();
			image = image.getScaledInstance((int)(image.getWidth(null)*0.8), /*(int)(imageH*0.8)*/ -1, Image.SCALE_SMOOTH);
		} else {
			image = new ImageIcon("textures/" + name + "_red.png").getImage();
			image = image.getScaledInstance((int)(image.getWidth(null)*0.8), /*(int)(imageH*0.8)*/ -1, Image.SCALE_SMOOTH);
		}
	}
	
}
