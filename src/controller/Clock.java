/**
 * 
 */
package controller;

/**
 * @author eeszsus
 *
 */
public class Clock extends Thread {
	int timeToSleep;
	boolean isRunning;
	private Controller controller;
	
	public Clock(int timeToSleep, Controller controller) {
		this.timeToSleep = timeToSleep;
		this.controller = controller;
		isRunning = false;
	}
	
	public void waitUntilNextTurn() throws InterruptedException {
		Thread.sleep(timeToSleep);
	}
	
	public void run() {
		isRunning = true;
		while(isRunning) {
			try {
				waitUntilNextTurn();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			controller.startNextTurn();
		}
	}
}
