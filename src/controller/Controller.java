/**
 * 
 */
package controller;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import communicationClient.MQTTClient;
import model.DirectionHistory;
import model.Node;
import model.NodeHTTP;
import model.NodeMQTT;
import model.Packet;
import model.Packet.BodyValue;
import model.Packet.Type;
import view.NodeView;
import view.PacketView;
import view.Coordinate;
import view.LegoWindow;

/**
 * @author eeszsus
 *
 */
public class Controller implements Observer {
	public enum Source {
		CONTROLLER, TRAFFICLIGHT
	}
	
	public ArrayList<Node> mQTTNodeList;
	private Clock clock;
	private CopyOnWriteArrayList<PacketView> mQttPacketViewList;
	private ArrayList<NodeView> mQTTNodeViewList;
	private ArrayList<PacketView> packetViewsToDelete;
	private LegoWindow window;
	private int doRepaint;
	private ArrayList<DirectionHistory> directionHistoryLog;
	private MQTTClient mQTTClient;
	private boolean replayMode;
	private long messageTime1;
	private long messageTime2;

	public static double MOVE_DISTANCE = 0.0002;
	public static int SLEEP = 1;
	public static int REPAINT = 10;
	public static double CARX = 0.8;
	public static double CARY = 0.8;
	/**
	 * 
	 */
	public Controller() {
		clock = new Clock(SLEEP, this);
		mQTTNodeList = new ArrayList<Node>();
		mQTTNodeViewList = new ArrayList<NodeView>();
		mQttPacketViewList = new CopyOnWriteArrayList<PacketView>();
		directionHistoryLog = new ArrayList<>();
		replayMode = false;
		packetViewsToDelete = new ArrayList<PacketView>(); 
		doRepaint = 0;
		messageTime1 = System.currentTimeMillis();
		messageTime2 = System.currentTimeMillis();
		makeMQTTMap();
		window = new LegoWindow(directionHistoryLog);
		window.addMQTTPanel(mQTTNodeViewList, mQttPacketViewList);
		window.addObserver(this);
		clock.start();
	}

	private void makeMQTTMap() {
		Node mobile = new NodeHTTP("mobile");
		NodeView mobileView = new NodeView(mobile, new Coordinate(0.2, 0.85));
		
		Node webServer = new NodeHTTP("webServer");
		NodeView nodeJSView = new NodeView(webServer, new Coordinate(0.2, 0.6));
		mobile.setNextNode(webServer);
		mobileView.setNextNodeView(nodeJSView);
		
		Node mQTTClient = new NodeMQTT("MQTTClient");
		NodeView cloudView = new NodeView(mQTTClient, new Coordinate(0.35, 0.33));
		webServer.setNextNode(mQTTClient);
		nodeJSView.setNextNodeView(cloudView);
		
		Node mQTTServer = new NodeMQTT("MQTTServer");
		NodeView mQTTServerView = new NodeView(mQTTServer, new Coordinate(0.5, 0.15));
		mQTTClient.setNextNode(mQTTServer);
		cloudView.setNextNodeView(mQTTServerView);
		
		Node legoTruck = new NodeMQTT("legoTruck");
		NodeView legoTruckView = new NodeView(legoTruck, new Coordinate(CARX, CARY));
		mQTTServer.setNextNode(legoTruck);
		mQTTServerView.setNextNodeView(legoTruckView);
		
		Node trafficLight = new NodeMQTT("trafficLight");
		NodeView trafficLightView = new NodeView(trafficLight, new Coordinate(0.5, 0.7));
		trafficLight.setNextNode(mQTTServer);
		trafficLightView.setNextNodeView(mQTTServerView);
		
		mQTTNodeList.add(mobile);
		mQTTNodeList.add(webServer);
		mQTTNodeList.add(mQTTClient);
		mQTTNodeList.add(trafficLight);
		mQTTNodeList.add(mQTTServer);
		mQTTNodeList.add(legoTruck);
		
		mQTTNodeViewList.add(mobileView);
		mQTTNodeViewList.add(nodeJSView);
		mQTTNodeViewList.add(cloudView);
		mQTTNodeViewList.add(trafficLightView);
		mQTTNodeViewList.add(mQTTServerView);
		mQTTNodeViewList.add(legoTruckView);
	}
	
	public void newMQTTPacket(BodyValue carDirection, Source source) {
		Node sourceNode;
		NodeView sourceNodeView; 
		if(source == Source.CONTROLLER) {
			sourceNode = mQTTNodeList.get(0);
			sourceNodeView = mQTTNodeViewList.get(0);
		} else {
			sourceNode = mQTTNodeList.get(3);
			sourceNodeView = mQTTNodeViewList.get(3); // TODO find by name
		}
		if(mQTTNodeList != null) {
			Packet packet = new Packet(Type.HTTP, carDirection, sourceNodeView.getDistance());
			sourceNode.addPacket(packet);
			PacketView packetView = new PacketView(packet, mQTTNodeViewList, this, MOVE_DISTANCE, 0);
			mQttPacketViewList.add(packetView);
		} else {
			System.out.println("MQTTnodeList is null");
		}
	}
	
	public void startNextTurn() {
		if(packetViewsToDelete != null) {
			for(PacketView packetView: packetViewsToDelete) {
				mQttPacketViewList.remove(packetView);
			}
			packetViewsToDelete.clear();
		}
		
		if(mQTTNodeList != null) {
			for(Node node : mQTTNodeList) {
				node.takeTurn(MOVE_DISTANCE);
			}
		}
		
		if(mQttPacketViewList != null) {
			for(PacketView packetView: mQttPacketViewList) {
				packetView.newPosition();
			}
		}
		
		if(doRepaint == 0) {
			window.repaintMap();
			doRepaint = REPAINT;
		} else {
			doRepaint--;
		}
	}
	
	public LegoWindow getWindow() {
		return window;
	}

	public void paintThings() {
	}

	public void deletePacketView(PacketView packetView) {
		packetViewsToDelete.add(packetView);
	}

	public void addToHistory(String direction) {
		if(replayMode == true) return;
		System.out.println("\"" + direction + "\"" + " added to directionHistory");
		long delay = messageTime2 - messageTime1;
		System.out.println("DELAY: " + delay);
		DirectionHistory directionLog = new DirectionHistory(direction, delay);
		directionHistoryLog.add(directionLog);
	}

	public ArrayList<DirectionHistory> getHistory() {
		return directionHistoryLog;
	}

	public void setMQTTClient(MQTTClient mQTTClient) {
		if(this.mQTTClient == null) {
			this.mQTTClient = mQTTClient;
			mQTTClient.addObserver(mQTTNodeViewList.get(3));
		}
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		replayMode = true;

        Queue<DirectionHistory> historyQueue = new LinkedList<DirectionHistory>(directionHistoryLog);
        System.out.println("history queue size: " + historyQueue.size());
		ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(1);
		
		doSend(exec, historyQueue, null);
	}
	
	public void doSend(ScheduledThreadPoolExecutor exec, Queue<DirectionHistory> historyQueue, String message) {
		if(message != null) {
			mQTTClient.sendMessage(message);
		}
		if(!historyQueue.isEmpty()) {
			DirectionHistory direction = historyQueue.poll();
			long duration = (long) direction.getDuration();
			String messageToSend = direction.getString();
			exec.schedule(new Runnable() {
				public void run() {
					doSend(exec, historyQueue, messageToSend);
				}
			}, duration, TimeUnit.MILLISECONDS);
		} else {
			directionHistoryLog.clear();
			replayMode = false;
		}
	}

	public boolean isReplayMode() {
		return replayMode;
	}
	
	public void setMessageTime(long milliTime) {
		messageTime1 = messageTime2;
		messageTime2 = milliTime;
	}
}
