/**
 * 
 */
package controller;

import javax.swing.SwingUtilities;

import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import communicationClient.MQTTClient;
//import communicationClient.SignalRClient;

/**
 * @author eeszsus
 *
 */
public class Main {
	static MQTTClient mQTTClient;
//	static SignalRClient signalRClient; 
	static Controller controller;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		controller = new Controller();
		mQTTClient = new MQTTClient(controller);
		mQTTClient.initClient();
		controller.setMQTTClient(mQTTClient);
		
		NativeInterface.initialize();
		NativeInterface.open();
		SwingUtilities.invokeLater(new Runnable() {
	        public void run() {
//	        	controller.addBrowser("testmobil.ddns.net/camera.html"); //("https://www.youtube.com/embed/Sj0Ha7Xkw7Y");
	        }
	    });
		NativeInterface.runEventPump();
		
	    // don't forget to properly close native components
	    Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
	        @Override
	        public void run() {
	            NativeInterface.close();
	        }
	    }));
	    
	}

}
