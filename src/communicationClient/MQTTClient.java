/**
 * 
 */
package communicationClient;

import java.util.ArrayList;
import java.util.Observable;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import controller.Controller;
import controller.Controller.Source;
import model.Packet.BodyValue;
/**
 * @author eeszsus
 *
 */
public class MQTTClient extends Observable implements MqttCallback{

	public MqttClient visualizeClient;
	private Controller controller;
	BodyValue lastMessage;
	Source sourceNode;
	
	public MQTTClient(Controller controller) {
		this.controller = controller;
		this.lastMessage = BodyValue.INIT;
	}

	public void initClient() {
		
        String broker       = "tcp://testmobil.ddns.net:1883";
        String clientId     = "Visualize";
        MemoryPersistence persistence = new MemoryPersistence();
		
		try {
            visualizeClient = new MqttClient(broker, clientId, persistence);
            visualizeClient.setCallback(this);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            System.out.println("Connecting to broker: " + broker);
            visualizeClient.connect(connOpts);
            visualizeClient.subscribe("/hackethon2017/#");
            System.out.println("Connected");
            
            try {
				Thread.sleep(3600);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
        } catch(Exception me) {
            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);
        }
	}
	
	public void stopClient() {
        try {
			visualizeClient.disconnect();
			System.out.println("Disconnected");
			System.exit(0);
		} catch (MqttException me) {
            System.out.println("reason "+me.getReasonCode());
            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);
			me.printStackTrace();
		}
	}
	
	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		
		System.out.println("message: " + message);
		BodyValue carDirection = getDirection(message);
		if(carDirection == null) {
			System.out.println("Faulty message arrived: " + message);
			return;
		}
		
		if(lastMessage == BodyValue.INIT) {
			controller.newMQTTPacket(carDirection, sourceNode);
			lastMessage = carDirection;
		} else {
			if(carDirection != lastMessage || controller.isReplayMode() == true) {
				controller.newMQTTPacket(carDirection, sourceNode);
				lastMessage = carDirection;
			}
		}
		
		if(controller.isReplayMode() == false) {
			controller.setMessageTime(System.currentTimeMillis());
			controller.addToHistory(message.toString());
		}
	}

	@Override
	public void connectionLost(Throwable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {
		// TODO Auto-generated method stub
		
	}
	
	private BodyValue getDirection(MqttMessage message) {
		String messageString = message.toString();
		sourceNode = Source.CONTROLLER;
		if (messageString.contains("RIGHT")){
			return BodyValue.RIGHT;
		} else if (messageString.contains("LEFT")) {
			return BodyValue.LEFT;
		} else if (messageString.contains("UP")) {
			
			this.setChanged();
			this.notifyObservers("white");
			
			return BodyValue.UP;
		} else if (messageString.contains("DOWN")) {
			return BodyValue.DOWN;
		} else if (messageString.contains("STOP")) {
			
			this.setChanged();
			this.notifyObservers("red");
			
			return BodyValue.STOP;
		} else if (messageString.contains("LAMP")) {
			return BodyValue.LAMP;
		} else if (messageString.contains("C_START")) {
			sourceNode = Source.TRAFFICLIGHT;
			
			this.setChanged();
			this.notifyObservers("white");
			
			return BodyValue.UP;
		} else if (messageString.contains("C_STOP")) {
			sourceNode = Source.TRAFFICLIGHT;
			
			this.setChanged();
			this.notifyObservers("red");
			
			return BodyValue.STOP;
		} else {
			return null;
		}
	}
	
	public void sendMessage(String message) {
		MqttMessage mqttMessage = new MqttMessage();
		mqttMessage.setPayload(message.getBytes());
		try {
			visualizeClient.publish("/hackethon2017/lego", mqttMessage);
			System.out.println("Sending direction " + "\"" + mqttMessage + "\"");
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}
}

