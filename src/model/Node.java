/**
 * 
 */
package model;

import java.util.ArrayList;

/**
 * @author eeszsus
 *
 */
public abstract class Node {
	Node nextNode;
	ArrayList<Packet> packetList;
	String name;
	
	public Node(String name) {
		this.name = name;
		packetList = new ArrayList<Packet>();
	}
	
	public void setNextNode(Node nextNode) {
		this.nextNode = nextNode;
	}
	
	public abstract void addPacket(Packet packet);
	
	public void takeTurn(double moveDistance) {
		if(packetList != null) {
			ArrayList<Packet> packetsToRemove = new ArrayList<Packet>();
			for (Packet packet : packetList) {
				packet.decreaseDistance(moveDistance);
				if(packet.getDistance() <= 0) {
					if(nextNode != null) {
						nextNode.addPacket(packet);
						packet.newCurrentNode();
					}
					packetsToRemove.add(packet);
				}
			}
			if(packetsToRemove != null) {
				for(Packet packetToRemove : packetsToRemove) {
					packetList.remove(packetToRemove);
				}
				packetsToRemove.clear();
			}
		}
	}

	public String getName() {
		return name;
	}	
	
}
