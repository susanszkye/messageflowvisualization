/**
 * 
 */
package model;

import model.Packet.Type;

/**
 * @author eeszsus
 *
 */
public class NodeMQTT extends Node {

	/**
	 * @param name
	 */
	public NodeMQTT(String name) {
		super(name);
	}

	@Override
	public void addPacket(Packet packet) {
		packetList.add(packet);
		packet.setType(Type.MQTT);
	//	System.out.println("Packet added to " + name + " and type set to " + packet.getTypeString());
	}

}
