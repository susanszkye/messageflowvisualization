package model;

public class DirectionHistory {
	String direction;
	long duration;
	
	
	public DirectionHistory(String direction, long duration) {
		this.direction = direction;
		this.duration = duration;
	}


	public String getString() {
		// TODO Auto-generated method stub
		return direction;
	}
	
	public double getDuration() {
		return duration;
	}

}
