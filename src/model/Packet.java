package model;

import java.util.Observable;

public class Packet extends Observable {
	public enum BodyValue {
		RIGHT, LEFT, UP, DOWN, STOP, INIT, LAMP
	}
	
	public enum Type {
		HTTP, MQTT
	}

	double distance;
	private BodyValue packetData;
	private Type packetType;
	private String sensorValue;
	
	public Packet(Type packetType, BodyValue direction, double distance) {
		this.packetType = packetType;
		this.packetData = direction;
		this.distance = distance;
		this.sensorValue = null;
	//	System.out.println("packet distance set to: " + this.distance);
	}	
	
	public Packet(Type packetType, BodyValue direction, double distance, String sensorValue) {
		this.packetType = packetType;
		this.packetData = direction;
		this.distance = distance;
		this.sensorValue = sensorValue;
	//	System.out.println("packet distance set to: " + this.distance);
	}
	
	public void decreaseDistance(double progress) {
		if(distance > 0) {
			distance -= progress;
			//System.out.println("Decrease distance to: " + distance);
		}
	}
	
	public BodyValue getBodyValue() {
		return packetData;
	}
	
	public double getDistance() {
		return distance;
	}

	public void setType(Type packetType) {
		this.packetType = packetType;
	}

	public String getTypeString() {
		return packetType.toString();
	}

	public Type getType() {
		return this.packetType;
	}
	
	public void setDistance(double newDistance) {
		this.distance = newDistance; 
	}

	public void newCurrentNode() {
	//	System.out.println("current node changed for packet, fireing event");
		this.setChanged();
		this.notifyObservers();	
	}

	public String getDirection() {
		return this.packetData.toString();
	}
	
	public String getSensorValue () {
		return sensorValue;
	}
}
