/**
 * 
 */
package model;

import model.Packet.Type;

/**
 * @author eeszsus
 *
 */
public class NodeHTTP extends Node {

	/**
	 * @param name
	 */
	public NodeHTTP(String name) {
		super(name);
	}

	@Override
	public void addPacket(Packet packet) {
		packetList.add(packet);
		packet.setType(Type.HTTP);
	//	System.out.println("Packet added to " + name + " and type set to " + packet.getTypeString());
	}

}
